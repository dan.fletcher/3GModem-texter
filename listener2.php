#!/usr/bin/env php
<?php

require_once('websockets.php');
require_once('users.php');
require_once('functions.php');
require_once('functions_sql.php');

global $serial, $response, $i;

$serial = connect();
$response = [];
$i = 0;

dbconn();


class caamsServer extends WebSocketServer {
  //protected $maxBufferSize = 1048576; //1MB... overkill for an echo server, but potentially plausible for other applications.


  protected function process ($user, $message) {
    global $serial, $response;

    print "Received from ".$user->id." : ".$message."\r\n";

    $receive = json_decode($message,true);

    switch($receive['event'])
    {
    case 'sendtext':
      // basic number formatting change to ensure it starts +447, goes to prevent sending texts to non uk mobiles.
      // our courier won't deliver them anyway.
      if(substr($receive['number'],0,4) != '+447')
      {
        $receive['number'] = "+447".substr($receive['number'],2);
      }
      $number = dbquery('select id from numbers where number = '.$receive['number'].' limit 1');

      if(isset($number[0]['id']))
      {
        $msgid =  dbinsert('messages',
        array('number_id' => $number[0]['id'], 'status' => 'Q', 'message' => $receive['msg'], 'date' => date("Y-m-d H:i:s")));
      }
      else
      {
        $numberid = dbinsert('numbers', array('number' => $receive['number'], 'status' => 'A'));
        $msgid = dbinsert('messages',
        array('number_id' => $numberid, 'status' => '', 'message' => $receive['msg'], 'date' => date("Y-m-d H:i:s")));
      }

	    put_command($serial, "AT+CMGS=\"".$receive['number']."\"".chr(13));
      put_command($serial, $receive['msg'].chr(26));
      break;

    case 'findall':
      $numberid = dbquery("select id from numbers where number = ".$receive['number']." LIMIT 1")[0]['id'];
      $all = dbquery("select id, message, date , status from messages where number_id = ".$numberid." ORDER BY date ASC");

      foreach($all as $item)
      {
        $json = ['event' => 'findall_history', 'id' => $item['id'], 'status' => $item['status'], 'message' => $item['message'], 'date' => $item['date']];
        $this->send($user,json_encode($json));
      }
      break;

    case 'read':
      $numberid = dbquery("select id from numbers where number = ".$receive['number']." LIMIT 1")[0]['id'];
      $all = dbquery("select id from messages where number_id = ".$numberid." AND status = 'U'");

      foreach($all as $item)
      {
        $json = ['event' => 'read', 'id' => $item['id']];
        dbupdate('messages', array('id' => $item['id'], 'status' => 'R'));
        foreach($this->users as $currentuser) {
          $this->send($currentuser,json_encode($json));
        }
      }
      break;

    default:
      break;
    }

    //global $serial;
    //put_command($serial, $message);
  }

  protected function tick (){
    global $serial, $response, $i;

    // go find new data
    $return = get_response($serial,$response);

    // check if we got anything back
    if ($return != "")
    {
	// merge new data
	$response = array_merge($response, $return);

	$loop = true;

	print "New data Received\n\r";

	print "Starting at : ".$i."\r\n";
	print_r($response);

	// start processing;
	while($loop)
	{
	switch (explode(":",$response[$i])[0])
	  {
	  case 'OK':
		print "thats an OK Message\r\n";
		unset($response[$i]);
		$i++;
		break;

    // this is the case type of sent message
	  case '+CMGS':
		$json = ['type' => 'sent'];
                foreach ($this->users as $currentuser) {
                //  $this->send($currentuser,json_encode($json));
                }
		unset($response[$i]);
		unset($response[$i+1]);
		unset($response[$i+2]);
                $i = $i+3;
		break;


	  case '+CMT':
		//+CMT: "+447930316503",,"17/09/13,19:05:59+04"

    		print "New Message\r\n";
		$phone = explode(",",explode(":",$response[$i])[1])[0];
		$phone = substr($phone,2,-1);
		$date = explode(",",$response[$i])[2];
    		$date = substr($date,1);
    		$time = explode(",",$response[$i])[3];
    		$time = substr($time,0,-4);
		$msg = substr($response[$i+1],0,-1);

		print "FROM ".$phone." AT ".$date." ".$time."\n\r";
    		unset($response[$i]);
		unset($response[$i+1]);
    		print "TEXT: ".$msg."\r\n";
    		unset($response[$i+2]);
    		$i = $i+3;

		$number = dbquery('select id, name from numbers where number = '.$phone.' limit 1');
		if(isset($number[0]['id']))
		{
		  $msgid = dbinsert('messages',
			  array('number_id' => $number[0]['id'], 'status' => 'U', 'message' => $msg, 'date' => $date." ".$time));
		}
		else
    {
		$numberid = dbinsert('numbers', array('number' => $phone, 'status' => 'A'));
      		$msgid = dbinsert('messages',
        	array('number_id' => $numberid, 'status' => 'U', 'message' => $msg, 'date' => $date." ".$time));
    }

		$json = ['event' => 'new', 'id' => $msgid, 'name' => $number[0]['name'], 'number' => $phone, 'date' => $date." ".$time, 'msg' => $msg];
		foreach ($this->users as $currentuser) {
          	  $this->send($currentuser,json_encode($json));
	       	}
                break;


	  default:
		//print $i;
		if(!$response[$i] = '\n')
		{
		  print "Unsure of this line : ".$response[$i];
		}

                unset($response[$i]);
                $i++;

		if(!isset($response[$i]))
		{
                  $loop = false;
		  $i = 0;
		}
		break;
	  }
	}
    }

  }

  protected function connected ($user) {
    print "User Connected : ".$user->id."\r\n";
    //$this->send($user, "Connected to messaging server".$user->id);
    $json = ['event' => 'status', 'message' => 'connected'];
    $this->send($user,json_encode($json));
    // Sent new user all unread messages

    //$this->send($user, "Sending Unread messages".$user->id);


    $unread = dbquery("select messages.id, numbers.name, numbers.number, messages.date, messages.message FROM messages LEFT JOIN numbers ON messages.number_id = numbers.id WHERE messages.status = 'U'");

    foreach ($unread as $new) {
      $json = ['event' => 'new', 'id' => $new['id'], 'name' => $new['name'], 'number' => $new['number'],'date' => $new['date'], 'msg' => $new['message']];
      $this->send($user,json_encode($json));
    }

  }

  protected function closed ($user) {
    print "User Disconencted : ".$user->id."\r\n";
  }
}

$echo = new caamsServer("192.168.1.3","9001");

try {
  $echo->run();
}
catch (Exception $e) {
  $echo->stdout($e->getMessage());
}
