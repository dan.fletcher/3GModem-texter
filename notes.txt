DB Config

create database textmsg;

use textmes;

(status A active, B blocked, D deleted)

create table numbers (id int AUTO_INCREMENT NOT NULL, number VARCHAR(15), name VARCHAR(50), status VARCHAR(1), timestamp TIMESTAMP, PRIMARY KEY (id), KEY (number));

(U unread, R read, S sent, E errored, D deleted, Q queuing)

create table messages (id int AUTO_INCREMENT NOT NULL, number_id int, status VARCHAR(1), message VARCHAR(1000), date TIMESTAMP, timestamp TIMESTAMP, PRIMARY KEY (id), KEY (number_id));

