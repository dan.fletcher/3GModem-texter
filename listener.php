#!/usr/bin/env php
<?php

require_once('config.php');
require_once('websockets.php');
require_once('users.php');
require_once('functions.php');

global $serial;

$serial = connect();


class Server extends WebSocketServer {

  protected function process ($user, $message) {
	echo $message;
        put_command($serial, "AT");
  }
	
  protected function tick () {
    global $serial;
    $response = get_response($serial);

    while($response != false ){
    print $response;
      //$json = json_encode($array);

      	if ($response === 'OK'){
		$msg = ['type'=> 'status', 'status' => 'OK'];
        	foreach ($this->users as $currentUser)
        	{
          	  $this->send($currentUser,json_encode($msg));
        	}
	}
	elseif(substr($response,1,4) === '+CMT:'){
		$tmp = substr($response,4,100);
                $msg = ['type' => 'new', 'from' => $tmp ];
                foreach ($this->users as $currentUser)
                {
                  $this->send($currentUser,json_encode($msg));
                }
        }
    $response = get_response($serial);
    }	
  }	 

  protected function connected ($user) {
    print "User Connected : ".$user->id;
//    $this->send($user,"Sending events...");
  }

  protected function closed ($user) {
   print "User Disconencted : ".$user->id;
  }
}

$echo = new Server("127.0.0.1","9001");

try {
  $echo->run();
}
catch (Exception $e) {
  $echo->stdout($e->getMessage());
}


?>
