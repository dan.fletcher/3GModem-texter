<?php

require('serial.php');

function connect()
{

	$port = "/dev/ttyUSB1";

	// create serial port 
	$serial = new phpSerial;

	$serial->deviceSet($port);
	$serial->confBaudRate(9600);
	$serial->confParity("none");
	$serial->confCharacterLength(8);
	$serial->confStopBits(1);
	$serial->confFlowControl("none");

	$serial->deviceOpen();

	// check working
	$serial->sendMessage("AT".chr(13));

	// change mode
	$serial->sendMessage("AT+CMGF=1".chr(13));
	// query mode chaged
	$serial->sendMessage("AT+CMGF=?".chr(13));

	// turn on notifications
	$serial->sendMessage("AT+CNMI=1,2,0,0,0".chr(13));

	return $serial;
}

function get_response($serial)
{
	$string = $serial->readPort();

	if ($string != "")
      	{
		return explode("\n", $string);
        }
}

function put_command($serial, $action)
{
        $serial->sendMessage($action);
}

function parse_response($response, $event)
{

        $exploded = explode("\r\n",$response);
        foreach ($exploded as $line)
        {
                if (substr($line,0,strlen($event)) == $event)
                        {
                                return substr($line, strlen($event)+2);
                        }
        }
}

// This function will turn the return string into an array
function response_array($response)
{
	$return = array();

	// split the string into multiple array
        $exploded = explode("\r\n",$response);

        foreach ($exploded as $line)
        {
        	// split the line by : key=value and tidy value
	        $arr = explode(":", $line);
		$return[$arr[0]] = substr($arr[1], 1);
        }
	return $return;
}

?>

